# 410™ Where everything existed

Status: **410** "this page existed" and [still is][1] ...

We are the akashic records of the internet

### you know, we know, everybody knows ...

and it is just here at your fingertips 

[akashic files](http://127.0.0.1:8080/ipns/webui.ipfs.io/#files)

[1]: http://duckduckgo.com/?q=immutable+existence


<!--
more on forgiveness...
 Evanescence of memories (bad-record etc.)
-->
